# Aardwolf-Plugins

## MUSHclient Plugins for Aardwolf MUD

* [Aardwolf MUD homepage](https://www.aardwolf.com/)
* [MUSHclient homepage](https://github.com/fiendish/aardwolfclientpackage)

### Aardwolf Consider Window FesterHead
This is my version of the Consider Window plugin by Korbus from November 2017, which in itself is from a 2014 update by Strazor.

* Re-orderdered trigger listing to match Aardwolf output from `help consider`
* Reset mob ranges based on Aardwolf output from `help consider`
